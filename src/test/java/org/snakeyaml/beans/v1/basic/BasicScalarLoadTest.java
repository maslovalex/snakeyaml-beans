/**
 * Copyright (c) 2019, http://www.snakeyaml.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snakeyaml.beans.v1.basic;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.snakeyaml.beans.v1.api.BeanLoad;
import org.snakeyaml.beans.v1.api.ClassDefinition;
import org.snakeyaml.engine.v2.api.LoadSettings;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Tag("fast")
class BasicScalarLoadTest {

    @Test
    void loadStringScalar() {
        LoadSettings settings = LoadSettings.builder().build();
        ClassDefinition rootContext = new ClassDefinition(String.class);

        BeanLoad<String> load = new BeanLoad<>(settings, rootContext);
        String scalar = load.loadBeanFromString("foo");
        assertEquals("foo", scalar);
    }

    @Test
    void loadBooleanScalar() {
        LoadSettings settings = LoadSettings.builder().build();
        ClassDefinition rootContext = new ClassDefinition(Boolean.class);

        BeanLoad<Boolean> load = new BeanLoad<>(settings, rootContext);
        Boolean scalar = load.loadBeanFromString("false");
        assertFalse(scalar);
    }

    @Test
    void loadBigIntegerScalar() {
        LoadSettings settings = LoadSettings.builder().build();
        ClassDefinition rootContext = new ClassDefinition(BigInteger.class);

        BeanLoad<BigInteger> load = new BeanLoad<>(settings, rootContext);
        BigInteger scalar = load.loadBeanFromString("12345678901234567890");
        assertEquals(new BigInteger("12345678901234567890"), scalar);
    }
}
