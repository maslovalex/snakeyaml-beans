/**
 * Copyright (c) 2019, http://www.snakeyaml.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.snakeyaml.beans.v1.basic;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.snakeyaml.beans.v1.api.BeanLoad;
import org.snakeyaml.beans.v1.api.ClassDefinition;
import org.snakeyaml.engine.v2.api.LoadSettings;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@Tag("fast")
class BasicLoadTest {

    @Test
    @DisplayName("Load the simplest JavaBean")
    void basicLoad() {
        LoadSettings settings = LoadSettings.builder().build();
        ClassDefinition rootContext = new ClassDefinition(BasicBean.class);
        BeanLoad<BasicBean> load = new BeanLoad<>(settings, rootContext);
        BasicBean bean = load.loadBeanFromString("name: Producer\nage: 21");
        assertNotNull(bean);
        assertEquals("Producer", bean.getName());
        assertEquals(21, bean.getAge());
    }

    @Test
    @DisplayName("Load the simplest JavaBean with null")
    void basicLoadWithNull() {
        LoadSettings settings = LoadSettings.builder().build();
        ClassDefinition rootContext = new ClassDefinition(OneFieldBean.class);
        BeanLoad<OneFieldBean> load = new BeanLoad<>(settings, rootContext);
        OneFieldBean bean = load.loadBeanFromString("theName: null");
        assertNotNull(bean);
        assertNull(bean.getTheName(), "Unexpected value '" + bean.getTheName() + "'");
    }
}
